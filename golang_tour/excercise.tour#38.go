package main

import (
	"code.google.com/p/go-tour/pic"
)

func Pic(dx, dy int) [][]uint8 {
	fs := make([][]uint8, dy)
	for i := 0; i < dy; i++ {
		fs[i] = make([]uint8, dx)
		for j := range fs[i] {
			fs[i][j] = uint8(i) * uint8(j)
		}
	}
	return fs
}

func main() {
	pic.Show(Pic)
}
