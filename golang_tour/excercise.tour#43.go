package main

import (
	"code.google.com/p/go-tour/wc"
	"strings"
)

func WordCount(s string) map[string]int {
	f := strings.Fields(s)
	y := make(map[string]int)
	for i := 0; i < len(f); i++ {
  		(y[f[i]])++
	}
	return y
}

func main() {
	wc.Test(WordCount)
}
