package main

import (
	"fmt"
	"math/cmplx"
)

func Cbrt(x complex128) complex128 {
	z := complex128(1)
	d := complex128(0)
	for {
		z = z - (cmplx.Pow(z, 3) - x) / (3*(z*z))
		if cmplx.Abs(d-z) < 1e-20 {
			break
		}
		d = z
	}
	return z
}

func main() {
	fmt.Println(Cbrt(2))
}
